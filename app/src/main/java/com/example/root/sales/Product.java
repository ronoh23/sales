package com.example.root.sales;

/**
 * Created by root on 6/17/17.
 */

public class Product {
    private String productName, productDescription, createdAt;
    private float price;
    private int isActive;
    private int id;

    public Product(String productName, String productDescription, Float price, int isActive, String createdAt, int id) {
        this.productName = productName;
        this.productDescription = productDescription;
        this.price = price;
        this.isActive = isActive;
        this.createdAt = createdAt;
        this.id =  id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName() {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription() {
        this.productDescription = productDescription;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice() {
        this.price = price;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive() {
        this.isActive = isActive;
    }

    public int getId() {
        return id;
    }

    public void setId() {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt() {
        this.createdAt = createdAt;
    }

}
