package com.example.root.sales;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SellProductActivity extends AppCompatActivity {

    EditText productIdTxt, productNameTxt, priceTxt, sellPriceTxt, profitTxt;
    Button sellProductBtn, closeDialogBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_product);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set the toolbar title
        String title = "Sell Product";
        getSupportActionBar().setTitle(title);

        // Receive data from product activity
        Intent i = getIntent();

        final int id = i.getExtras().getInt("PRODUCT_ID");
        final String productName = i.getExtras().getString("PRODUCT_NAME");
        final Float price = i.getExtras().getFloat("PRODUCT_PRICE");

        productIdTxt = (EditText) findViewById(R.id.productIdTxt);
        productNameTxt = (EditText) findViewById(R.id.productNameTxt);
        priceTxt = (EditText) findViewById(R.id.priceTxt);
        sellPriceTxt = (EditText) findViewById(R.id.sellPriceTxt);
        profitTxt = (EditText) findViewById(R.id.profitTxt);


        sellProductBtn = (Button) findViewById(R.id.sellProductBtn);
        closeDialogBtn = (Button) findViewById(R.id.closeDialogBtn);

        // Assign data to those views
        productIdTxt.setText(String.valueOf(id));
        productNameTxt.setText(productName);
        priceTxt.setText(String.valueOf(price));


        // get params from view to sell product
        String pId = productIdTxt.getText().toString();
        String pName = productNameTxt.getText().toString();
        final String buyAmount = priceTxt.getText().toString();
        final String sellAmount = sellPriceTxt.getText().toString();
        final String profit = profitTxt.getText().toString();


        // calculate profit automatically on sellAmount edit text change
        sellPriceTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Float sellAmount = Float.parseFloat(sellPriceTxt.getText().toString());
                Float buyAmount = Float.parseFloat(priceTxt.getText().toString());
                Float profit = (sellAmount - buyAmount);
                profitTxt.setText(profit.toString());
            }
        });

        // insert into sales table
        sellProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pIdS = productIdTxt.getText().toString().trim();
                String pName = productNameTxt.getText().toString().trim();
                String amountS = priceTxt.getText().toString().trim();
                String profitS = profitTxt.getText().toString().trim();

                if (!pIdS.isEmpty() && !pName.isEmpty() && !amountS.isEmpty() && !profitS.isEmpty()) {
                    Integer pId = Integer.parseInt(pIdS);
                    Float amount = Float.parseFloat(amountS);
                    Float profit = Float.parseFloat(profitS);
                    saveSale(pId, pName, amount, profit);
                } else {
                    Toast.makeText(getApplicationContext(), "Please enter sale details!", Toast.LENGTH_LONG).show();
                }


            }
        });

        // end activity on click close
        closeDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void saveSale(int pId, String pName, Float amount, Float profit) {
        DBAdapter db = new DBAdapter(this);

        // Open
        db.openDB();

        // Insert

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String createdAt = date.toString();

        long result = db.addSale(pId, pName, amount, profit, createdAt);
        long result1 = db.nullifyProduct(pId);

        if(result>0 && result1>0) {
            Toast.makeText(this, "Sale Added Successfully.", Toast.LENGTH_SHORT).show();
            this.finish();
        } else {
            Toast.makeText(this, "Error in adding sale please try again", Toast.LENGTH_SHORT).show();
        }

        // Close db
        db.close();

    }


}
