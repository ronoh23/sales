package com.example.root.sales;

/**
 * Created by root on 7/3/17.
 */

public class ProductSpinnerObject {

    private int productId;
    private String productName;
    private Float amount;

    public ProductSpinnerObject(int productId, String productName, Float amount) {
        this.productId = productId;
        this.productName = productName;
        this.amount = amount;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public Float getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return productName;
    }
}
