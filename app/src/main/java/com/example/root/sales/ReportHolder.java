package com.example.root.sales;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by root on 6/29/17.
 */

public class ReportHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView saleCountTxt, revenueTxt, profitTxt, monthTxt;
    ItemClickListener itemClickListener;

    public ReportHolder(View itemView) {
        super(itemView);

        //Assign
        saleCountTxt = (TextView) itemView.findViewById(R.id.salesCountTxt);
        revenueTxt = (TextView) itemView.findViewById(R.id.revenueTxt);
        profitTxt = (TextView) itemView.findViewById(R.id.profitTxt);
        monthTxt = (TextView) itemView.findViewById(R.id.monthTxt);

        itemView.setOnClickListener(this);
    }

    @Override

    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener = ic;
    }
}
