package com.example.root.sales;

/**
 * Created by root on 6/29/17.
 */

public class Report {
    private String month;
    private Float profit, revenue;
    private int saleCount;

    public Report(String month, int saleCount, Float revenue, Float profit) {
        this.saleCount = saleCount;
        this.revenue = revenue;
        this.profit = profit;
        this.month = month;
    }

    public int getSaleCount() {
        return saleCount;
    }

    public void setSaleCount(int saleId) {
        this.saleCount = saleCount;
    }

    public Float getRevenue() {
        return revenue;
    }

    public void setRevenue(Float revenue) {
        this.revenue = revenue;
    }

    public Float getProfit() {
        return profit;
    }

    public void setProfit(Float profit) {
        this.profit = profit;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }


}
