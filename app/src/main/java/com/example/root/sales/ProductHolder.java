package com.example.root.sales;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by root on 6/20/17.
 */

public class ProductHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView productNameTxt, productDescriptionTxt, priceTxt, productIdTxt;
    Button sellProductBtn;
    ItemClickListener itemClickListener;

    public ProductHolder(View itemView) {
        super(itemView);

        // Assign
        productNameTxt        = (TextView) itemView.findViewById(R.id.productNameTxt);
        productDescriptionTxt = (TextView) itemView.findViewById(R.id.productDescriptionTxt);
        priceTxt              = (TextView) itemView.findViewById(R.id.priceTxt);
        sellProductBtn        = (Button) itemView.findViewById(R.id.sellProductBtn);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener=ic;
    }
}

