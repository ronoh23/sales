package com.example.root.sales;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by root on 6/20/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, Constants.DB_NAME, null, Constants.DB_VERSION);
    }

    // When table is created
    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Constants.CREATE_PRODUCTS_TABLE);
            db.execSQL(Constants.CREATE_SALE_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Upgrade Table
    @Override
    public void onUpgrade(SQLiteDatabase db , int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS"+Constants.PRODUCT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS"+Constants.SALE_TABLE_NAME);
        onCreate(db);
    }
}
