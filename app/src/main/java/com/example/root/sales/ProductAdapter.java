package com.example.root.sales;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Created by root on 6/20/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {
    Context c;
    ArrayList<Product> products;
    ProductAdapter adapter;


    public ProductAdapter(Context ctx, ArrayList<Product> products) {
        // Assign them locally
        this.c = ctx;
        this.products = products;
    }


    public void refresh(ArrayList<Product> products){
        this.products = products;
        notifyDataSetChanged();
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // View Obj from xml
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_model, null);

        // holder
        ProductHolder holder = new ProductHolder(v);
        // Adapter
        adapter = new ProductAdapter(c, products);

        return holder;
    }


    public interface OnItemLongClickListener{
        void onLongClick();
    }

    OnItemLongClickListener onItemLongClickListener;
    // Bind data to views
    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {
        holder.productNameTxt.setText(products.get(position).getProductName());
        holder.productDescriptionTxt.setText(products.get(position).getProductDescription());
        holder.priceTxt.setText(products.get(position).getPrice().toString());

        holder.productDescriptionTxt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final int id = products.get(position).getId();
                AlertDialog diaBox = AskOption(id);
                diaBox.show();

                notifyDataSetChanged();
                adapter.refresh(products);

                return false;
            }
        });

        // Handle item clicks
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                // Open detail activity
                // Pass data
                // Create intent
                Intent i = new Intent(c, ProductDetailsActivity.class);

                i.putExtra("PRODUCT_NAME", products.get(pos).getProductName());
                i.putExtra("PRODUCT_DESCRIPTION", products.get(pos).getProductDescription());
                i.putExtra("PRODUCT_PRICE", products.get(pos).getPrice());
                i.putExtra("PRODUCT_ID", products.get(pos).getId());

                String productName = products.get(pos).getProductName();
                String productDescription = products.get(pos).getProductDescription();
                Float price = products.get(pos).getPrice();
                Integer id = products.get(pos).getId();

                Log.v("Extras","Product name is "+productName);
                Log.v("Extras","Product description is "+productDescription);
                Log.v("Extras","Product price is "+price);
                Log.v("Extras","Product ID is "+id);

                // Start activity
                c.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    // AlertDialog prompt before delete
    private AlertDialog AskOption(final int id) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(c)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete this product?")
                //.setIcon(R.drawable.delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        deleteProduct(id);
                        dialog.dismiss();
                        adapter.refresh(products);
                    }

                })



                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    // method to delete product

    private void deleteProduct(int id) {
        DBAdapter db = new DBAdapter(c);
        db.openDB();

        long result = db.deleteProduct(id);

        if(result>0) {
            //this.finish();
        } else {
            Toast.makeText(c, "Unable to delete product", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    // method to retrieve products and refresh adapter
    public void retrieveProducts() {
        DBAdapter db = new DBAdapter(c);

        // Open
        db.openDB();

        products.clear();

        // Select
        Cursor c = db.getAllProducts();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {
            int id = c.getInt(0);
            String productName = c.getString(1);
            String productDescription = c.getString(2);
            Float price = c.getFloat(3);
            Integer isActive = c.getInt(4);
            String createdAt = c.getString(5);

            // Create new product
            Product p = new Product(productName, productDescription, price, isActive, createdAt, id);

            // Add to product
            products.add(p);
        }

        // Set adapter to rv
        if(!(products.size()<1)) {
            adapter.refresh(products);
        }
    }
}
