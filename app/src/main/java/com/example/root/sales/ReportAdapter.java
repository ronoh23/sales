package com.example.root.sales;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.root.sales.Sale;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

/**
 * Created by root on 6/29/17.
 */

public class ReportAdapter extends RecyclerView.Adapter<ReportHolder> {
    Context c;
    ArrayList<Report> reports;
    ReportAdapter adapter;

    public ReportAdapter(Context ctx, ArrayList<Report> reports) {
        // Assign them locally
        this.c = ctx;
        this.reports = reports;
    }

    public void refresh(ArrayList<Report> reports) {
        this.reports = reports;
        notifyDataSetChanged();
    }


    @Override
    public ReportHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // View obj from xml
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_model, null);

        // holder
        ReportHolder holder = new ReportHolder(v);
        // Adapter
  //      ReportAdapter adapter = new ReportAdapter(c, reports);

        return holder;
    }

    // Bind data to views
    @Override
    public void onBindViewHolder(ReportHolder holder, final int position) {
        // get month name
        String month = getMonth(Integer.parseInt(reports.get(position).getMonth()));

        holder.saleCountTxt.setText(""+reports.get(position).getSaleCount());
        holder.profitTxt.setText(reports.get(position).getProfit().toString());
        holder.revenueTxt.setText(reports.get(position).getRevenue().toString());
        //holder.monthTxt.setText(reports.get(position).getMonth().toString());
        holder.monthTxt.setText(month);

    }


    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }
}
