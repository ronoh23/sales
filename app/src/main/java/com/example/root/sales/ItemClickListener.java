package com.example.root.sales;

import android.view.View;

/**
 * Created by root on 6/20/17.
 */

public interface ItemClickListener {
    void onItemClick(View v, int pos);
}
