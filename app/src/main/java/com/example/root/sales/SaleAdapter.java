package com.example.root.sales;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.root.sales.Sale;

import java.util.ArrayList;

/**
 * Created by root on 6/29/17.
 */

public class SaleAdapter extends RecyclerView.Adapter<SaleHolder> {
    Context c;
    ArrayList<Sale> sales;
    SaleAdapter adapter;

    public SaleAdapter(Context ctx, ArrayList<Sale> sales) {
        // Assign them locally
        this.c = ctx;
        this.sales = sales;
    }

    public void refresh(ArrayList<Sale> sales){
        this.sales = sales;
        notifyDataSetChanged();
    }

    public interface OnItemLongClickListener{
        void onLongClick();
    }

    @Override
    public SaleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // View obj from xml
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_model, null);

        // holder
        SaleHolder holder = new SaleHolder(v);
        // Adapter
        adapter = new SaleAdapter(c, sales);

        return holder;
    }

    // Bind data to views
    @Override
    public void onBindViewHolder(SaleHolder holder, final int position) {
        //holder.pIdTxt.setText(sales.get(position).getpId());
        holder.pNameTxt.setText(sales.get(position).getpName());
        holder.amountTxt.setText(sales.get(position).getAmount().toString());
        holder.profitTtx.setText(sales.get(position).getProfit().toString());

        // Handle Item Clicks
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                // Open sales activity
                // Pass data
                // Create Intent
                Intent i = new Intent(c, SalesDetailsActivity.class);

                // Load data
                i.putExtra("SALE_ID", sales.get(pos).getId());
                /*i.putExtra("P_ID", sales.get(pos).getpId());*/
                i.putExtra("P_NAME", sales.get(pos).getpName());
                i.putExtra("AMOUNT", sales.get(pos).getAmount());
                i.putExtra("PROFIT", sales.get(pos).getProfit());

                // Start Activity
                c.startActivity(i);
            }
        });

        holder.amountTxt .setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final int id = sales.get(position).getId();
                AlertDialog diaBox = AskOption(id);
                diaBox.show();

                notifyDataSetChanged();
                adapter.refresh(sales);

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return sales.size();
    }

    private AlertDialog AskOption(final int id) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(c)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete this product?")
                //.setIcon(R.drawable.delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        deleteSale(id);
                        dialog.dismiss();
                        adapter.refresh(sales);
                    }

                })



                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    // delete sale
    // Delete

    private void deleteSale(int id) {
        DBAdapter db = new DBAdapter(c);
        db.openDB();

        long result = db.deleteSale(id);

        if(result>0) {
            //this.finish();
        } else {
            Toast.makeText(c, "Unable to delete sale", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

}
