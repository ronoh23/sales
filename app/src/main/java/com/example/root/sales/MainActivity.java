package com.example.root.sales;



import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.root.sales.activity.FragmentDrawer;
import com.example.root.sales.activity.ProductsFragment;
import com.example.root.sales.activity.HomeFragment;
import com.example.root.sales.activity.ReportsFragment;
import com.example.root.sales.activity.SalesFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;

    ArrayList<Sale> sales = new ArrayList<>();
    Spinner pIdTxt;
    Button SalesButton, ProductsButton, ReportsButton;
    EditText pNameTxt, amountTxt, profitTxt, sellPriceTxt;
    EditText productNameTxt,productDescriptionTxt, priceTxt, isActiveTxt;
    SaleAdapter saleAdapter;
    ProductAdapter productAdapter;
    ArrayList<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        productAdapter = new ProductAdapter(this,products);
        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_product) {
            showProductsDialog();

            //Toast.makeText(getApplicationContext(), "Add product action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(), "Action settings is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        if(id == R.id.action_add_sale){
            showSalesDialog();

            //Toast.makeText(getApplicationContext(), "Add sale action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.title_home);
                break;
            case 1:
                fragment = new ProductsFragment();
                title = getString(R.string.title_products);
                break;
            case 2:
                fragment = new SalesFragment();
                title = getString(R.string.title_sales);
                break;
            case 3:
                fragment = new ReportsFragment();
                title = getString(R.string.title_reports);
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

    // SALES SECTION //

    // load spinner data for products
    private void loadProductSpinnerData() {
        // db handler
        DBAdapter db = new DBAdapter(this);

        // spinner drop down elements
        final List<ProductSpinnerObject> productLabels = (List<ProductSpinnerObject>) db.getProducts();

        // creating adapter for spinner
        ArrayAdapter<ProductSpinnerObject> productDataAdapter = new ArrayAdapter<ProductSpinnerObject>(this, android.R.layout.simple_spinner_dropdown_item, productLabels);

        // drop down layout style - list view with radio button
        productDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        if(productLabels.isEmpty()) {
            pIdTxt.setPrompt("Opps! no products in stock");
        }

        pIdTxt.setPrompt("Choose Product");
        pIdTxt.setAdapter(productDataAdapter);
    }

    // custom dialog to add new sale
    public void showSalesDialog() {

        final AlertDialog.Builder bd = new AlertDialog.Builder(this);

        View d = View.inflate(this,R.layout.custom_sale_layout,null);
        bd.setView(d);

        bd.setTitle("Add New Sale");

        pIdTxt = (Spinner) d.findViewById(R.id.pIdSpinnerEditTxt);
        sellPriceTxt  = (EditText) d.findViewById(R.id.sellPriceEditText);
        profitTxt = (EditText) d.findViewById(R.id.profitEditText);


        Button saveSaleBtn = (Button) d.findViewById(R.id.saveSaleBtn);
        Button closeDialogBtn = (Button) d.findViewById(R.id.closeDialogBtn);

        pIdTxt.setPrompt("Choose Product");

        // calculate profit automatically on sellAmount edit text change
        sellPriceTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    Float sellAmount = Float.parseFloat(sellPriceTxt.getText().toString());
                    final Float buyAmount = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getAmount ();
                    Float profit = (sellAmount - buyAmount);
                    profitTxt.setText(profit.toString());
                } catch (NumberFormatException e) {
                    profitTxt.setText("0.00");
                }

            }
        });

        bd.setPositiveButton("Save", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {

                int pId = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductId ();
                String pName = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductName ();
                final Float buyAmount = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getAmount ();

                final String Amount = buyAmount.toString();
                Log.v("Extras","Product price is "+Amount);

                String sellPriceS = sellPriceTxt.getText().toString().trim();

                if(!sellPriceS.isEmpty()) {
                    Float sellPrice = Float.parseFloat(sellPriceS);
                    Float total = (sellPrice - buyAmount);
                    saveSale(pId, pName, sellPrice, total);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter sale details!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        bd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                retrieveSales();
                dialog.dismiss();

            }
        });



        // onClicks
        saveSaleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pId = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductId ();
                String pName = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductName ();
                final Float buyAmount = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getAmount ();

                final String Amount = buyAmount.toString();
                Log.v("Extras","Product price is "+Amount);

                String sellPriceS = sellPriceTxt.getText().toString().trim();

                if(!sellPriceS.isEmpty()) {
                    Float sellPrice = Float.parseFloat(sellPriceS);
                    Float total = (sellPrice - buyAmount);
                    saveSale(pId, pName, sellPrice, total);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter sale details!", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });

        closeDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveSales();

            }
        });
        loadProductSpinnerData();

        // Show dialog
        bd.show();
    }

    // retrieve sales records from db
    public void retrieveSales() {
        DBAdapter db = new DBAdapter(this);

        // Open
        db.openDB();

        sales.clear();

        // Select
        Cursor c = db.getAllSales();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {
            int id = c.getInt(0);
            int pId = c.getInt(1);
            String pName = c.getString(2);
            Float amount = c.getFloat(3);
            Float profit = c.getFloat(4);
            String createdAt = c.getString(5);

            // Create new sale
            Sale s = new Sale(pId, pName, amount, profit, createdAt, id);

            // Add to sale
            sales.add(s);
        }

        // Set adapter to rv
        if(!(sales.size()<1)) {
            //saleAdapter.refresh(sales);
        }
    }

    // method to add new sale to db
    public void saveSale(int pId, String pName, Float amount, Float profit) {
        DBAdapter db = new DBAdapter(getApplicationContext());

        // Open
        db.openDB();

        // Insert

        String createdAt = getDateTime();

        long result = db.addSale(pId, pName, amount, profit, createdAt);
        long result1 = db.nullifyProduct(pId);
        if(result>0 && result1>0) {
            //pIdTxt.setText("");
            //pNameTxt.setText("");
            //amountTxt.setText("");
            profitTxt.setText("");
            Toast.makeText(getApplicationContext(), "Sale Added Successfully.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Error in adding sale please try again", Toast.LENGTH_SHORT).show();
        }

        // Close db
        db.close();

        // Refresh
        //retrieveSales();
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    // PRODUCTS SECTION //
    public void showProductsDialog() {

        final AlertDialog.Builder bd = new AlertDialog.Builder(this);

        View d = View.inflate(this,R.layout.custom_product_layout,null);
        bd.setView(d);

        bd.setTitle("Add New Product");

        productNameTxt        = (EditText) d.findViewById(R.id.productNameEditTxt);
        productDescriptionTxt = (EditText) d.findViewById(R.id.productDescriptionEditTxt);
        priceTxt              = (EditText) d.findViewById(R.id.priceEditTxt);

        Button saveProductBtn = (Button) d.findViewById(R.id.saveProductBtn);
        Button closeDialogBtn = (Button) d.findViewById(R.id.closeDialogBtn);

        bd.setPositiveButton("Save", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                String productName = productNameTxt.getText().toString().trim();
                String productDescription = productDescriptionTxt.getText().toString().trim();
                String amount = priceTxt.getText().toString().trim();

                if(!productName.isEmpty() && !productDescription.isEmpty() && !amount.isEmpty()) {
                    Float price = Float.parseFloat(amount);
                    saveProduct(productName, productDescription, price);
                    retrieveProducts();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter product details!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        bd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                retrieveProducts();
                dialog.dismiss();

            }
        });

        // onClicks
        saveProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productName = productNameTxt.getText().toString().trim();
                String productDescription = productDescriptionTxt.getText().toString().trim();
                String amount = priceTxt.getText().toString().trim();

                if(!productName.isEmpty() && !productDescription.isEmpty() && !amount.isEmpty()) {
                    Float price = Float.parseFloat(amount);
                    saveProduct(productName, productDescription, price);
                    retrieveProducts();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter product details!", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });

        closeDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveProducts();

            }
        });

        // Show dialog
        bd.show();
    }

    public void saveProduct(String productName, String productDescription, Float price) {
        DBAdapter db = new DBAdapter(this);

        // Open
        db.openDB();

        // Insert

        String createdAt = getDateTime();
        Integer isActive = 1;

        long result = db.addProduct(productName, productDescription, price, isActive, createdAt);

        if(result>0) {
            productNameTxt.setText("");
            productDescriptionTxt.setText("");
            priceTxt.setText("");
            Toast.makeText(getApplicationContext(), "Product Added Successfully.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Error in adding product please try again", Toast.LENGTH_SHORT).show();
        }

        // Close db
        db.close();

        // Refresh
        //retrieveProducts();
    }

    public void retrieveProducts() {
        DBAdapter db = new DBAdapter(this);

        // Open
        db.openDB();

        products.clear();

        // Select
        Cursor c = db.getAllProducts();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {
            int id = c.getInt(0);
            String productName = c.getString(1);
            String productDescription = c.getString(2);
            Float price = c.getFloat(3);
            Integer isActive = c.getInt(4);
            String createdAt = c.getString(5);

            // Create new product
            Product p = new Product(productName, productDescription, price, isActive, createdAt, id);

            // Add to product
            products.add(p);
        }

        // Set adapter to rv
        if(!(products.size()<1)) {
            productAdapter.refresh(products);
        }
    }
}