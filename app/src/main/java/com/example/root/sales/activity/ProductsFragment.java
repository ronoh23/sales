package com.example.root.sales.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import android.app.Dialog;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.example.root.sales.DBAdapter;
import com.example.root.sales.Product;
import com.example.root.sales.ProductAdapter;
import com.example.root.sales.R;

import java.util.ArrayList;
import java.util.Date;


public class ProductsFragment extends Fragment {

    TextView productsCountTxt;
    EditText productNameTxt,productDescriptionTxt, priceTxt, isActiveTxt;
    RecyclerView rv;
    ProductAdapter adapter;
    ArrayList<Product> products = new ArrayList<>();
    Button addProductBtn;





    public ProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        retrieveProducts();
        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_products, container, false);

        // Get products count
        DBAdapter db = new DBAdapter(getActivity());
        db.openDB();

        int profile_counts = db.getProductsCount();
        db.close();

        // Get text view
        productsCountTxt = (TextView) rootView.findViewById(R.id.productsCountTxt);

        // Set text view
        productsCountTxt.setText(String.valueOf(profile_counts+" Products"));

        Button addProductBtn = (Button) rootView.findViewById(R.id.addProductBtn);

        addProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        // Recycler
        rv = (RecyclerView) rootView.findViewById(R.id.productsRecycler);

        // Set its properties
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setItemAnimator(new DefaultItemAnimator());

        // Adapter
        adapter = new ProductAdapter(getActivity(), products);

        rv.setAdapter(adapter);

        retrieveProducts();

        adapter.refresh(products);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }





    public void showDialog() {

        final AlertDialog.Builder bd = new AlertDialog.Builder(getActivity());

        View d = View.inflate(getActivity(),R.layout.custom_product_layout,null);
        bd.setView(d);

        bd.setTitle("Add New Product");

        bd.setPositiveButton("Save", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {
                String productName = productNameTxt.getText().toString().trim();
                String productDescription = productDescriptionTxt.getText().toString().trim();
                String amount = priceTxt.getText().toString().trim();

                if(!productName.isEmpty() && !productDescription.isEmpty() && !amount.isEmpty()) {
                    Float price = Float.parseFloat(amount);
                    saveProduct(productName, productDescription, price);
                } else {
                    Toast.makeText(getActivity(),
                            "Please enter product details!", Toast.LENGTH_LONG)
                            .show();
                    bd.show();
                }
            }

        });

        bd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                retrieveProducts();
                dialog.dismiss();

            }
        });

        productNameTxt        = (EditText) d.findViewById(R.id.productNameEditTxt);
        productDescriptionTxt = (EditText) d.findViewById(R.id.productDescriptionEditTxt);
        priceTxt              = (EditText) d.findViewById(R.id.priceEditTxt);

        Button saveProductBtn = (Button) d.findViewById(R.id.saveProductBtn);
        Button closeDialogBtn = (Button) d.findViewById(R.id.closeDialogBtn);

        // onClicks
        saveProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productName = productNameTxt.getText().toString().trim();
                String productDescription = productDescriptionTxt.getText().toString().trim();
                String amount = priceTxt.getText().toString().trim();

                if(!productName.isEmpty() && !productDescription.isEmpty() && !amount.isEmpty()) {
                    Float price = Float.parseFloat(amount);
                    saveProduct(productName, productDescription, price);
                } else {
                    Toast.makeText(getActivity(),
                            "Please enter product details!", Toast.LENGTH_LONG)
                            .show();
                }


            }
        });

        closeDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveProducts();

            }
        });

        // Show dialog
        bd.show();
    }

    public void saveProduct(String productName, String productDescription, Float price) {
        DBAdapter db = new DBAdapter(getActivity());

        // Open
        db.openDB();

        // Insert

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String createdAt = date.toString();
        Integer isActive = 1;

        long result = db.addProduct(productName, productDescription, price, isActive, createdAt);

        if(result>0) {
            productNameTxt.setText("");
            productDescriptionTxt.setText("");
            priceTxt.setText("");
            Toast.makeText(getActivity(), "Product Added Successfully.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Error in adding product please try again", Toast.LENGTH_SHORT).show();
        }

        // Close db
        db.close();

        // Refresh
        retrieveProducts();
    }

    public void retrieveProducts() {
        DBAdapter db = new DBAdapter(getActivity());

        // Open
        db.openDB();

        products.clear();

        // Select
        Cursor c = db.getAllProducts();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {
            int id = c.getInt(0);
            String productName = c.getString(1);
            String productDescription = c.getString(2);
            Float price = c.getFloat(3);
            Integer isActive = c.getInt(4);
            String createdAt = c.getString(5);

            // Create new product
            Product p = new Product(productName, productDescription, price, isActive, createdAt, id);

            // Add to product
            products.add(p);
        }

        // Set adapter to rv
        if(!(products.size()<1)) {
            adapter.refresh(products);
        }
    }

}