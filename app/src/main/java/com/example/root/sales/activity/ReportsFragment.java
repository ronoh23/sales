package com.example.root.sales.activity;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.root.sales.DBAdapter;
import com.example.root.sales.R;
import com.example.root.sales.Report;
import com.example.root.sales.ReportAdapter;
import com.example.root.sales.Sale;
import com.example.root.sales.SaleAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class ReportsFragment extends Fragment {
    TextView dateTxt, productsCountTxt, salesCountTxt, totalRevenueTxt, totalProfitTxt, stockValueTxt;
    RecyclerView rv;
    ReportAdapter adapter;
    ArrayList<Report> reports = new ArrayList<>();
    public ReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);

        // Get products count
        DBAdapter db = new DBAdapter(getActivity());
        db.openDB();

        int profile_counts = db.getProductsCount();
        int sales_counts = db.getSalesCount();

        float total_revenue = db.getAmount();
        float total_profit = db.getProfit();
        float stock_value = db.getStockValue();
        db.close();

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());

        // Get text view
        dateTxt = (TextView) rootView.findViewById(R.id.dateTxt);

        // Set text view
        dateTxt.setText(String.valueOf(formattedDate));


        // Recycler
        rv = (RecyclerView) rootView.findViewById(R.id.reportsRecycler);

        // Set its properties
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setItemAnimator(new DefaultItemAnimator());

        // Adapter
        adapter = new ReportAdapter(getActivity(), reports);

        rv.setAdapter(adapter);

        retrieveReports();

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void retrieveReports() {
        DBAdapter db = new DBAdapter(getActivity());

        // Open
        db.openDB();

        reports.clear();

        // Select
        Cursor c = db.getReports();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {

            String month = c.getString(0);
            int saleCount = c.getInt(1);
            Float revenue = c.getFloat(2);
            Float profit = c.getFloat(3);

            Log.v("EXTRAS", "Month : " +month);
            Log.v("EXTRAS", "Sale Count : " +saleCount);
            Log.v("EXTRAS", "Revenue : " +revenue);
            Log.v("EXTRAS", "Profit : " +profit);
            // Create new sale
            Report s = new Report(month, saleCount, revenue, profit );

            // Add to reports
            reports.add(s);
        }

        // Set adapter to rv
        if(!(reports.size()>0)) {
            adapter.refresh(reports);
        }
    }
}