package com.example.root.sales;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by root on 6/29/17.
 */

public class SaleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView pIdTxt, pNameTxt, amountTxt, profitTtx;
    ItemClickListener itemClickListener;

    public SaleHolder(View itemView) {
        super(itemView);

        //Assign
        //pIdTxt = (TextView) itemView.findViewById(R.id.pIdTxt);
        pNameTxt = (TextView) itemView.findViewById(R.id.pNameTxt);
        amountTxt = (TextView) itemView.findViewById(R.id.amountTxt);
        profitTtx = (TextView) itemView.findViewById(R.id.profitTxt);

        itemView.setOnClickListener(this);
    }

    @Override

    public void onClick(View v) {
        this.itemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener ic) {
        this.itemClickListener = ic;
    }
}
