package com.example.root.sales;

/**
 * Created by root on 6/29/17.
 */

public class Sale {
    private String pName, createdAt;
    private Float profit, amount;
    private int pId, id;

    public Sale(int pId, String pName, Float amount, Float profit, String createdAt, int id) {
        this.pId = pId;
        this.pName = pName;
        this.amount = amount;
        this.profit = profit;
        this.id = id;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Float getProfit() {
        return profit;
    }

    public void setProfit(Float profit) {
        this.profit = profit;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
