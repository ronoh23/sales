package com.example.root.sales;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SalesDetailsActivity extends AppCompatActivity {

    EditText pIdTxt, pNameTxt, amountTxt, profitTxt;
    Button updateSaleBtn, deleteSaleBtn;
    Context c;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // set the toolbar title
        String title = "Edit Sales Details";
        getSupportActionBar().setTitle(title);

        // Receive data from product activity
        Intent i = getIntent();

        final String pId = i.getExtras().getString("P_ID");
        final String pName = i.getExtras().getString("P_NAME");
        final Float amount = i.getExtras().getFloat("AMOUNT");
        final Float profit = i.getExtras().getFloat("PROFIT");
        final int id = i.getExtras().getInt("SALE_ID");

        updateSaleBtn = (Button) findViewById(R.id.updateSaleBtn);
        deleteSaleBtn = (Button) findViewById(R.id.deleteSaleBtn);

        pIdTxt = (EditText) findViewById(R.id.pIdEditTxt);
        pNameTxt = (EditText) findViewById(R.id.pNameEditText);
        amountTxt = (EditText) findViewById(R.id.amountEditText);
        profitTxt = (EditText) findViewById(R.id.profitEditText);


        //Log.v("Extras","Product Id is "+pId);
        Log.v("Extras","Product name is "+pName);
        Log.v("Extras","Sale amount is "+amount);
        Log.v("Extras","Sale profit is "+profit);
        Log.v("Extras","Sale ID is "+id);

        // Assign data to those views
        //pIdTxt.setText(pId);
        pNameTxt.setText(pName);
        amountTxt.setText(String.valueOf(amount));
        profitTxt.setText(String.valueOf(profit));

        // Update
        updateSaleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Integer pId = Integer.parseInt(pIdTxt.getText().toString());
                Float amount = Float.parseFloat(amountTxt.getText().toString());
                Float profit = Float.parseFloat(profitTxt.getText().toString());
                updateSale(id, amount,profit);
            }
        });

        // Delete
        deleteSaleBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //deleteSale(id);
                AlertDialog diaBox = AskOption(id);
                diaBox.show();
            }
        });
    }


    private void updateSale(int id, Float newAmount, Float newProfit) {
        DBAdapter db = new DBAdapter(this);
        db.openDB();

        long result = db.updateSale(id, newAmount, newProfit);

        if(result>0) {
            //pIdTxt.setText(newPId);
           // pNameTxt.setText(newPname);
            amountTxt.setText(String.valueOf(newAmount));
            profitTxt.setText(String.valueOf(newProfit));

            Toast.makeText(getApplicationContext(), "Sale updated successfully", Toast.LENGTH_SHORT).show();
            this.finish();

        } else {
            Toast.makeText(getApplicationContext(), "Unable to update sale", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    // Delete

    private void deleteSale(int id) {
        DBAdapter db = new DBAdapter(this);
        db.openDB();

        long result = db.deleteSale(id);

        if(result>0) {
            this.finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unable to delete sale", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    private AlertDialog AskOption(final Integer id)
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete this sale?")
                //.setIcon(R.drawable.delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        deleteSale(id);
                        dialog.dismiss();
                    }

                })



                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

}
