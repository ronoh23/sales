package com.example.root.sales.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.app.Dialog;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.example.root.sales.DBAdapter;
import com.example.root.sales.Product;
import com.example.root.sales.ProductAdapter;
import com.example.root.sales.ProductSpinnerObject;
import com.example.root.sales.R;
import com.example.root.sales.SaleAdapter;
import com.example.root.sales.Sale;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class SalesFragment extends Fragment {
    TextView totalAmountTxt, totalProfitTxt, totalSalesTxt;
    Spinner pIdTxt;
    EditText pNameTxt, amountTxt, profitTxt, sellPriceTxt;
    RecyclerView rv;
    SaleAdapter adapter;
    ArrayList<Sale> sales = new ArrayList<>();
    Button addSaleBtn;

    public SalesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        retrieveSales();
        super.onResume();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sales, container, false);

        // Get products count
        DBAdapter db = new DBAdapter(getActivity());
        db.openDB();

        int sales_counts = db.getSalesCount();
        db.close();

        TextView totalSalesTxt = (TextView) rootView.findViewById(R.id.totalSalesTxt);

        // Set text view
        totalSalesTxt.setText(String.valueOf(sales_counts+" Sales"));

        Button addSaleBtn = (Button) rootView.findViewById(R.id.addSaleBtn);


        addSaleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        // Recycler
        rv = (RecyclerView) rootView.findViewById(R.id.salesRecycler);

        // Set its properties
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.setItemAnimator(new DefaultItemAnimator());

        // Adapter
        adapter = new SaleAdapter(getActivity(), sales);

        rv.setAdapter(adapter);

        retrieveSales();

        adapter.refresh(sales);


        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void loadProductSpinnerData() {
        // db handler
        DBAdapter db = new DBAdapter(getActivity());

        // spinner drop down elements
        List<ProductSpinnerObject> productLabels = (List<ProductSpinnerObject>) db.getProducts();

        // creating adapter for spinner
        ArrayAdapter<ProductSpinnerObject> productDataAdapter = new ArrayAdapter<ProductSpinnerObject>(getActivity(), android.R.layout.simple_spinner_dropdown_item, productLabels);

        // drop down layout style - list view with radio button
        productDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        pIdTxt.setAdapter(productDataAdapter);
    }


    public void showDialog() {

        final AlertDialog.Builder bd = new AlertDialog.Builder(getActivity());

        View d = View.inflate(getActivity(),R.layout.custom_sale_layout,null);
        bd.setView(d);

        bd.setTitle("Add New Sale");

        pIdTxt = (Spinner) d.findViewById(R.id.pIdSpinnerEditTxt);
        sellPriceTxt  = (EditText) d.findViewById(R.id.sellPriceEditText);
        profitTxt = (EditText) d.findViewById(R.id.profitEditText);


        Button saveSaleBtn = (Button) d.findViewById(R.id.saveSaleBtn);
        Button closeDialogBtn = (Button) d.findViewById(R.id.closeDialogBtn);

        // calculate profit automatically on sellAmount edit text change
        sellPriceTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    Float sellAmount = Float.parseFloat(sellPriceTxt.getText().toString());
                    final Float buyAmount = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getAmount ();
                    Float profit = (sellAmount - buyAmount);
                    profitTxt.setText(profit.toString());
                } catch (NumberFormatException e) {
                    profitTxt.setText("0.00");
                }

            }
        });

        bd.setPositiveButton("Save", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int whichButton) {

                int pId = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductId ();
                String pName = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getProductName ();
                final Float buyAmount = ( (ProductSpinnerObject) pIdTxt.getSelectedItem () ).getAmount ();

                final String Amount = buyAmount.toString();
                Log.v("Extras","Product price is "+Amount);

                String sellPriceS = sellPriceTxt.getText().toString().trim();

                if(!sellPriceS.isEmpty()) {
                    Float sellPrice = Float.parseFloat(sellPriceS);
                    Float total = (sellPrice - buyAmount);
                    saveSale(pId, pName, sellPrice, total);
                } else {
                    Toast.makeText(getActivity(),
                            "Please enter sale details!", Toast.LENGTH_LONG)
                            .show();
                    bd.show();
                }
            }

        });

        bd.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                retrieveSales();
                dialog.dismiss();

            }
        });


        closeDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retrieveSales();

            }
        });
        loadProductSpinnerData();

        // Show dialog
        bd.show();
    }

    public void saveSale(int pId, String pName, Float amount, Float profit) {
        DBAdapter db = new DBAdapter(getActivity());

        // Open
        db.openDB();

        // Insert

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();

        String createdAt = date.toString();

        long result = db.addSale(pId, pName, amount, profit, createdAt);
        long result1 = db.nullifyProduct(pId);

        if(result>0 && result1>0) {
            profitTxt.setText("");
            Toast.makeText(getActivity(), "Sale Added Successfully.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Error in adding sale please try again", Toast.LENGTH_SHORT).show();
        }

        // Close db
        db.close();

        // Refresh
        retrieveSales();
    }


    public void retrieveSales() {
        DBAdapter db = new DBAdapter(getActivity());

        // Open
        db.openDB();

        sales.clear();

        // Select
        Cursor c = db.getAllSales();

        // Loop through the data adding to arraylist
        while (c.moveToNext()) {
            int id = c.getInt(0);
            int pId = c.getInt(1);
            String pName = c.getString(2);
            Float amount = c.getFloat(3);
            Float profit = c.getFloat(4);
            String createdAt = c.getString(5);

            // Create new sale
            Sale s = new Sale(pId, pName, amount, profit, createdAt, id);

            // Add to sale
            sales.add(s);
        }

        // Set adapter to rv
        if(!(sales.size()<1)) {
            adapter.refresh(sales);
        }
    }

}