package com.example.root.sales;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 6/20/17.
 */

public class DBAdapter {
    Context c;
    SQLiteDatabase db;
    DBHelper helper;

    public DBAdapter(Context ctx) {
        this.c=ctx;
        helper=new DBHelper(c);
    }

    // Open DB
    public DBAdapter openDB() {
        try{
            db = helper.getWritableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return this;
    }

    // Close DB
    public void close() {
        try {
            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Insert data to products table
    public long addProduct(String productName, String productDescription, Float price, Integer isActive, String createdAt) {
        try {
            ContentValues cv = new ContentValues();

            cv.put(Constants.PRODUCT_NAME, productName);
            cv.put(Constants.PRODUCT_DESCRIPTION, productDescription);
            cv.put(Constants.PRICE, price);
            cv.put(Constants.IS_ACTIVE, isActive);
            cv.put(Constants.CREATED_AT, createdAt);

            return db.insert(Constants.PRODUCT_TABLE_NAME, Constants.PRODUCT_ID, cv);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // Insert data to sales table
    public long addSale(Integer pId, String pName, Float amount, Float profit, String createdAt) {
        try {
            ContentValues cv = new ContentValues();

            cv.put(Constants.P_ID, pId);
            cv.put(Constants.P_NAME, pName);
            cv.put(Constants.AMOUNT, amount);
            cv.put(Constants.PROFIT, profit);
            cv.put(Constants.CREATED_AT, createdAt);

            return db.insert(Constants.SALE_TABLE_NAME, Constants.SALE_ID, cv);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    final String active = "1";
    // Retrieve all products
    public Cursor getAllProducts() {
        String[] columns={Constants.PRODUCT_ID, Constants.PRODUCT_NAME, Constants.PRODUCT_DESCRIPTION, Constants.PRICE, Constants.IS_ACTIVE, Constants.CREATED_AT};

        //return db.query(Constants.PRODUCT_TABLE_NAME, columns,null,null,null,null,null);
        return db.query(Constants.PRODUCT_TABLE_NAME, columns, "is_active=?", new String[] { active }, null, null, null);
    }

    // Retrieve all sales
    public Cursor getAllSales() {
        String[] columns={Constants.SALE_ID, Constants.P_ID, Constants.P_NAME, Constants.AMOUNT, Constants.PROFIT, Constants.CREATED_AT};

        return db.query(Constants.SALE_TABLE_NAME, columns,null,null,null,null,null);
    }

    // Update product details

    public long updateProduct(int id, String productName, String productDescription, Float price, Integer isActive) {
        try {
            ContentValues cv = new ContentValues();

            cv.put(Constants.PRODUCT_NAME, productName);
            cv.put(Constants.PRODUCT_DESCRIPTION, productDescription);
            cv.put(Constants.PRICE, price);
            cv.put(Constants.IS_ACTIVE, isActive);

            return db.update(Constants.PRODUCT_TABLE_NAME,cv,Constants.PRODUCT_ID+" =?", new String[]{String.valueOf(id)});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public long nullifyProduct(int id) {
        try {
            ContentValues cv = new ContentValues();
            int active = 0;
            cv.put(Constants.IS_ACTIVE, active);

            return db.update(Constants.PRODUCT_TABLE_NAME,cv,Constants.PRODUCT_ID+" =?", new String[]{String.valueOf(id)});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // Update sale details

    public long updateSale(int id, Float amount, Float profit) {
        try {
            ContentValues cv = new ContentValues();

            //cv.put(Constants.P_ID, pId);
            //cv.put(Constants.P_NAME, pName);
            cv.put(Constants.AMOUNT, amount);
            cv.put(Constants.PROFIT, profit);

            return db.update(Constants.SALE_TABLE_NAME,cv,Constants.SALE_ID+" =?", new String[]{String.valueOf(id)});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // Delete product
    public long deleteProduct(int id) {
        try{
            return db.delete(Constants.PRODUCT_TABLE_NAME,Constants.PRODUCT_ID+" =?", new String[]{String.valueOf(id)});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // Delete sale
    public long deleteSale(int id) {
        try{
            return db.delete(Constants.SALE_TABLE_NAME,Constants.SALE_ID+" =?", new String[]{String.valueOf(id)});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // count no of products in db
    public int getProductsCount() {
        String countQuery = "SELECT  * FROM " + Constants.PRODUCT_TABLE_NAME + " WHERE is_active = 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    // count no of products in db
    public int getSalesCount() {
        String countQuery = "SELECT  * FROM " + Constants.SALE_TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    // get sum of all prices of products in stock
    public float getStockValue() {
        String countQuery = "SELECT sum(price) FROM " + Constants.PRODUCT_TABLE_NAME + " WHERE is_active = 1 ";
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        float i = cursor.getFloat(0);
        cursor.close();
        return i;
    }

    // get sum of profit sales
    public float getProfit() {
        String countQuery = "SELECT sum(profit) FROM " + Constants.SALE_TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        float i = cursor.getFloat(0);
        cursor.close();
        return i;
    }

    // get sum of amount sales
    public float getAmount() {
        String countQuery = "SELECT sum(amount) FROM " + Constants.SALE_TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();
        float i = cursor.getFloat(0);
        cursor.close();
        return i;
    }

    // Spinner drop down for products
    public List<ProductSpinnerObject> getProducts() {
        db = helper.getWritableDatabase();

        List <ProductSpinnerObject> productLabels = new ArrayList<ProductSpinnerObject>();
        // select query
        String selectQuery = "SELECT * FROM " + Constants.PRODUCT_TABLE_NAME + " WHERE is_active = 1 ";
        //String[] columns={Constants.PRODUCT_ID, Constants.PRODUCT_NAME};
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all the rows and adding to list
        if(cursor.moveToFirst()) {
            do {
                productLabels.add(new ProductSpinnerObject(cursor.getInt(0), cursor.getString(1), cursor.getFloat(3)));
            } while (cursor.moveToNext());
        }

        // closing the connection
        //cursor.close();
        //db.close();

        return productLabels;
    }

    // Retrieve reports
    public Cursor getReports() {

        String countQuery = "SELECT strftime('%m', created_at), count(id) as items_sold,  sum(amount) as revenue, sum(profit) as profit FROM " + Constants.SALE_TABLE_NAME + " GROUP BY strftime('%m', created_at) ";

        return db.rawQuery(countQuery, null);
    }
}
