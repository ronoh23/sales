package com.example.root.sales;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ProductDetailsActivity extends AppCompatActivity {

    EditText productNameTxt, productDescriptionTxt, priceTxt;
    Button updateProductBtn, deleteProductBtn, sellProductBtn;
    Context c;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // set the toolbar title
        String title = "Edit Product Details";
        getSupportActionBar().setTitle(title);

        // Receive data from product activity
        Intent i = getIntent();

        final String productName = i.getExtras().getString("PRODUCT_NAME");
        final String productDescription = i.getExtras().getString("PRODUCT_DESCRIPTION");
        final Float price = i.getExtras().getFloat("PRODUCT_PRICE");
        final int id = i.getExtras().getInt("PRODUCT_ID");

        updateProductBtn = (Button) findViewById(R.id.updateProductBtn);
        deleteProductBtn = (Button) findViewById(R.id.deleteProductBtn);
        sellProductBtn   = (Button) findViewById(R.id.sellProductBtn);

        productNameTxt = (EditText) findViewById(R.id.productNameEditTxt);
        productDescriptionTxt = (EditText) findViewById(R.id.productDescriptionEditTxt);
        priceTxt = (EditText) findViewById(R.id.priceEditTxt);


        Log.v("Extras","Product name is "+productName);
        Log.v("Extras","Product description is "+productDescription);
        Log.v("Extras","Product price is "+price);
        Log.v("Extras","Product ID is "+id);

        // Assign data to those views
        productNameTxt.setText(productName);
        productDescriptionTxt.setText(productDescription);
        priceTxt.setText(String.valueOf(price));

        // Update
        updateProductBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float price = Float.parseFloat(priceTxt.getText().toString());
                updateProduct(id,productNameTxt.getText().toString(), productDescriptionTxt.getText().toString(), price);
            }
        });

        // Delete
        // Update
        deleteProductBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //deleteProduct(id);
                AlertDialog diaBox = AskOption(id);
                diaBox.show();
            }
        });

        sellProductBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                // Create intent
                Intent i = new Intent(getApplicationContext(), SellProductActivity.class);

                i.putExtra("PRODUCT_NAME", productNameTxt.getText().toString());
                i.putExtra("PRODUCT_ID", id);
                i.putExtra("PRODUCT_PRICE", price);

                Log.v("Extras","Product name is "+productName);
                Log.v("Extras","Product ID is "+id);
                Log.v("Extras","Product Price is "+price);

                // Start activity
                startActivity(i);
            }
        });
    }


    private void updateProduct(int id, String newProductName, String newProductDescription, Float newPrice) {
        DBAdapter db = new DBAdapter(this);
        db.openDB();

        Integer isActive = 1;
        long result = db.updateProduct(id, newProductName, newProductDescription, newPrice, isActive);

        if(result>0) {
            productNameTxt.setText(newProductName);
            productDescriptionTxt.setText(newProductDescription);
            priceTxt.setText(String.valueOf(newPrice));

            Toast.makeText(getApplicationContext(), "Product updated successfully", Toast.LENGTH_SHORT).show();
            this.finish();

        } else {
            Toast.makeText(getApplicationContext(), "Unable to update product", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    // Delete

    private void deleteProduct(int id) {
        DBAdapter db = new DBAdapter(this);
        db.openDB();

        long result = db.deleteProduct(id);

        if(result>0) {
            this.finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unable to delete product", Toast.LENGTH_SHORT).show();
        }

        db.close();
    }

    private AlertDialog AskOption(final Integer id) {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setTitle("Delete")
                .setMessage("Are you sure you want to delete this product?")
                //.setIcon(R.drawable.delete)

                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        deleteProduct(id);
                        dialog.dismiss();
                    }

                })



                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                })
                .create();
        return myQuittingDialogBox;

    }

}
