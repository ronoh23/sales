package com.example.root.sales;

/**
 * Created by root on 6/20/17.
 */

public class Constants {
    // Columns for products table
    static final String PRODUCT_ID="id";
    static final String PRODUCT_NAME="product_name";
    static final String PRODUCT_DESCRIPTION="product_description";
    static final String PRICE="price";
    static final String IS_ACTIVE="is_active";
    static final String CREATED_AT="created_at";

    // Columns for sales table
    static final String SALE_ID="id";
    static final String P_ID="pId";
    static final String P_NAME="pName";
    static final String AMOUNT="amount";
    static final String PROFIT="profit";



    // Db properties
    static final String DB_NAME="sales";
    static final String PRODUCT_TABLE_NAME="products";
    static final String SALE_TABLE_NAME="sales";
    static int DB_VERSION='1';

    // Create products table
    static final String CREATE_PRODUCTS_TABLE="CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT, product_name TEXT NOT NULL, product_description TEXT NOT NULL, price FLOAT NOT NULL, is_active INTEGER NOT NULL, created_at DATETIME NOT NULL);";

    // Create sales table
    static final String CREATE_SALE_TABLE="CREATE TABLE sales(id INTEGER PRIMARY KEY AUTOINCREMENT, pId INTEGER NOT NULL, pName TEXT NOT NULL, amount FLOAT NOT NULL, profit FLOAT NOT NULL, created_at DATETIME NOT NULL);";

}
