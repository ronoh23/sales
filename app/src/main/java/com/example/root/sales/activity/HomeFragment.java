package com.example.root.sales.activity;

/**
 * Created by root on 6/2/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.root.sales.DBAdapter;
import com.example.root.sales.R;


public class HomeFragment extends Fragment {
    TextView productsCountTxt, salesCountTxt;
    Button SalesButton, ProductsButton, ReportsButton;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        // Get products count
        DBAdapter db = new DBAdapter(getActivity());
        db.openDB();

        int profile_counts = db.getProductsCount();
        int sales_counts = db.getSalesCount();
        db.close();

        ProductsButton = (Button) rootView.findViewById(R.id.ProductsButton);
        SalesButton = (Button) rootView.findViewById(R.id.SalesButton);
        ReportsButton = (Button) rootView.findViewById(R.id.ReportsButton);

        // start products fragment on click
        ProductsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new ProductsFragment();
                String title = getString(R.string.title_products);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();

            }
        });

        // start sales fragment on click
        SalesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new SalesFragment();
                String title = getString(R.string.title_sales);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();

            }
        });

        // start reports fragment on click
        ReportsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new ReportsFragment();
                String title = getString(R.string.title_reports);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, fragment);
                fragmentTransaction.commit();

            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}